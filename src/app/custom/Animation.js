import Monster from "./Monster";
import EventEmitter from "eventemitter3";

export default class Animation extends EventEmitter {
  constructor() {
    super();
    this.monsters = [];
    this.createMonaters();
  }
  static get events() {
    return {
      EXPANDED: "expanded",
      RESET: "reset",
    };
  }
  createMonaters() {
    const elements = document.getElementsByClassName("monster");

    for (const el of elements) {
      const monster = new Monster(el);
      this.monsters.push(monster);
      monster.on(Animation.events.EXPANDED, () => {
        for (const el of this.monsters) {
          el.contract();
        }
      });
      monster.on(Animation.events.RESET, () => {
        for (const el of this.monsters) {
          el.reset();
        }
      });
    }
  }
}
