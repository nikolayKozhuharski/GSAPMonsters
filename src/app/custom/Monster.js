import gsap from "gsap/all";
import EventEmitter from "eventemitter3";

export default class Monster extends EventEmitter {
  constructor(domElement) {
    super();
    this.domElement = domElement;
    this.expanded = false;
    domElement.addEventListener("mouseenter", () => this.expand());
    domElement.addEventListener("mouseleave", () =>
      this.emit(Monster.events.RESET)
    );
  }
  static get events() {
    return {
      EXPANDED: "expanded",
      RESET: "reset",
    };
  }
  expand() {
    this.emit(Monster.events.EXPANDED);
    const tween = gsap.to(this.domElement, { width: "80%", duration: 0.5, id: "expand" });
  }

  contract() {
    const tween = gsap.to(this.domElement, { width: "4%", duration: 0.5, id: "contract"});
  }

  reset() {
    const tween = gsap.to(this.domElement, { width: "16.6%", id: "reset"});
  }
}
